package main

import (
	"encoding/json"
	"log"
	"net/http"
)

func main() {
	handler := http.HandlerFunc(handleRequest)
	healthCheckHandler := http.HandlerFunc(healthCheckHandler)
	http.Handle("/", handler)
	http.Handle("/healthz", healthCheckHandler)
	http.ListenAndServe(":8080", nil)
}

func handleRequest(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]string)
	resp["tree"] = "sequoia"
	resp["is_favorite"] = "true"
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	}

	switch r.Method {
		case http.MethodGet:
			w.Write(jsonResp)	
		default:
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}

	return
}

func healthCheckHandler(w http.ResponseWriter, r *http.Request) {
    // A very simple health check.
    w.WriteHeader(http.StatusOK)
    w.Header().Set("Content-Type", "application/json")

    // In the future we could report back on the status of our DB, or our cache 
    // (e.g. Redis) by performing a simple PING, and include them in the response.
    // io.WriteString(w, `{"alive": true}`)
}