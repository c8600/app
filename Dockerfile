FROM --platform=linux/amd64 golang:1.18-alpine as builder

# Required to perform tests
RUN apk add --no-cache gcc musl-dev

WORKDIR /app

COPY *.go go.mod ./

RUN go test ./... -cover && \
    go build -o /favourite-tree

FROM --platform=linux/amd64 alpine:3.15 as app

COPY --from=builder /favourite-tree /usr/local/bin/favourite-tree

ENTRYPOINT ["favourite-tree"]