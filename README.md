# app

Hello Ecosia!

```bash
echo "Hello Ecosia!"
```

> Hello There!

## Another article

There should be docs

## Tasks

Top priority:

0. Double check there is a consistent build (container image)
1. Deploy to dev
2. Add UT
3. Add coverage (add `allow_failure`)
4. Add SAST
5. Add linters (code is priority, (Dockerfile, Helm, etc the second priority))

Backlog/Improvements:

- Add docs genration (pages, static site, swagger)
- Add more quality gates (SonarQube?) that shutdown (fail) the pipeline
- Container vulnurability scanning
- AT pipeline (component testing) -> dedicated, isolated env + cleanup
- E2E tests
